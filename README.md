# Py Lambda Rest

PyLambdaRest is a Python library for implementing HTTP Rest endpoints using AWS API Gateway and AWS Lambda.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install git+https://bitbucket.org/carol_lorini/py_lambda_rest
```

## Changelog

View recent changes here:
[CHANGELOG.md](CHANGELOG.md)

## Development environment setup

```bash
python3.13 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
