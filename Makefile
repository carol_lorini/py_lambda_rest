# Common python commands

venv:
	python3.13 -m venv venv

dev:
	pip install -r requirements.txt

pylint:
	pylint --fail-under=10 py_lambda_rest/

pylint-test:
	pylint --fail-under=10 -d missing-class-docstring,missing-function-docstring,duplicate-code,too-many-public-methods test/

ruff:
	ruff check

lint: ruff pylint pylint-test

coverage:
	coverage html

tests:
	green -vv -r -m 95 test

t: tests coverage
