# Changelog

## [1.13.0] - 2024-05-27

### Changed

- Bump dependencies to latest revision.

## [1.12.0] - 2024-02-13

### Changed

- Use built-in HTTPMethod enum.
- Add type annotations.

## [1.11.0] - 2024-01-22

### Changed

- Return HTTP status 401 - Unauthorized when authentication token is missing.

## [1.10.0] - 2024-01-12

### Changed

- Upgraded dependencies to latest version.

## [1.9.0] - 2023-09-03

### Changed

- Upgraded dependencies to latest version.

## [1.8.0] - 2023-05-01

### Changed

- Log event details on unexpected error in AbstractEndpoint.

## [1.7.0] - 2023-02-20

### Changed

- Upgraded jsonschema to version 4.17.3
- Upgraded PyJWT to version 2.6.0

## [1.6.0] - 2021-07-13

### Changed

- Upgraded PyJWT to version 2.1.0
- Improved AbstractEndpoint logs

## [1.5.0] - 2021-02-12

### Changed

- Upgraded PyJWT to version 2.0.1

## [1.4.5] - 2020-07-16

### Added

- Added property to access underlying Action function

## [1.4.4] - 2020-07-08

### Changed

- Execute request action inside method so that it can be overridden

## [1.4.3] - 2020-06-25

### Fixed

- Fixed error caused by debug logs

## [1.4.2] - 2020-06-02

### Added

- Log request response when debug level is activated

## [1.4.1] - 2020-03-05

### Changed

- Fixed falsey values in response body were being serialized to None instead of JSON

## [1.4.0] - 2020-02-24

### Added

- Handle multi value query parameters in Request class

## [1.3.0] - 2020-02-05

### Added

- Handle dataclasses in JSON encoder

## [1.2.0] - 2020-01-02

### Added

- Forbidden request exception class

### Changed

- Handle forbidden request errors in AbstractEndpoint
- Use CorsConfigurator class to setup CORS. Deprecated enable_cors and disable_cors functions.

## [1.1.0] - 2020-01-01

### Added

- JWT request builder class to help making request that use JWT tokens for authentication
- Forbidden HTTP request builder

## [1.0.2] - 2019-12-27

### Changed

- Updated library dependencies

## [1.0.1] - 2019-10-02

### Changed

- Encode JSON dates in the ISO 8601 format

## [1.0.0] - 2019-09-14

### Added

- JwtRequest used to decode JWT tokens
- Default values when getting request parameters
