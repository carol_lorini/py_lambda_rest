"""Logger configuration module"""
import logging
import os

LOGGER = logging.getLogger('py_lambda_rest')
LOGGER.setLevel(getattr(logging, os.environ.get('py_lambda_rest_log_level', 'INFO')))
