"""Common REST exceptions"""


class InvalidHttpStatusCode(Exception):
    """Raised when the value is not a valid HTTP status code"""


class MissingHttpStatusCode(Exception):
    """Raised when the HTTP status code has not been set"""


class ForbiddenRequestError(Exception):
    """Raised when the request is not authorized by the server"""


class ObjectNotFoundError(Exception):
    """Raised when the requested object was not found"""

    def __init__(self, resource, resource_id):
        super().__init__(f'{resource} {resource_id} not found')


class MissingToken(Exception):
    """Raised when using a class that requires a token but the token was not found"""


class InvalidToken(Exception):
    """Raise when there was an error decoding the token"""
