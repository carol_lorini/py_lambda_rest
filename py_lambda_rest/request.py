"""HTTP request abstractions"""
# pylint: disable=too-many-instance-attributes
import json
from http import HTTPMethod

import jwt

from .exceptions import InvalidToken, MissingToken


class Request:
    """Class that represents a HTTP request."""

    def __init__(self, event):
        self._resource = event["resource"]
        self._path = event["path"]
        self._method = HTTPMethod[event["httpMethod"]]
        self._headers = self._get_value_if_not_null(event, "headers", {})
        self._query_parameters = self._get_value_if_not_null(event, "queryStringParameters", {})
        self._multi_value_query_parameters = self._get_value_if_not_null(
            event, "multiValueQueryStringParameters", {}
        )
        self._path_parameters = self._get_value_if_not_null(event, "pathParameters", {})
        self._is_base64 = event["isBase64Encoded"]
        body = event["body"]
        self._body = json.loads(body) if body and not self._is_base64 else body

    @property
    def resource(self) -> str:
        """The request resource"""
        return self._resource

    @property
    def path(self) -> str:
        """The request path"""
        return self._path

    @property
    def method(self) -> HTTPMethod:
        """The request HTTP method"""
        return self._method

    @property
    def headers(self) -> dict:
        """The request headers"""
        return self._headers

    @property
    def is_base64(self) -> bool:
        """Indicates if the body is base 64 encoded."""
        return self._is_base64

    def get_header(self, header_name: str, default_value=None):
        """
        Gets the value of the specified header.
        Returns the default value if the header doesn't exist.
        """
        return self._headers.get(header_name, default_value)

    @property
    def query_parameters(self) -> dict:
        """The request query parameters"""
        return self._query_parameters

    def get_query_parameter(self, parameter_name: str, default_value=None):
        """
        Gets the value of a query parameter.
        Returns the default value if the parameter doesn't exist.
        """
        return self._query_parameters.get(parameter_name, default_value)

    @property
    def multi_value_query_parameters(self) -> dict:
        """The request's multi value query parameters."""
        return self._multi_value_query_parameters

    def get_multi_value_query_parameter(self, parameter_name: str, default_value=None):
        """
        Gets the list of values of a query parameter.
        Returns the default value if the parameter doesn't exist.
        """
        return self._multi_value_query_parameters.get(parameter_name, default_value)

    @property
    def body(self):
        """The request body"""
        return self._body

    @property
    def path_paramters(self) -> dict:
        """The request path parameters"""
        return self._path_parameters

    def get_path_parameter(self, parameter_name: str, default_value=None):
        """
        Gets the value of a path parameter.
        Returns the default value if the parameter doesn't exist.
        """
        return self._path_parameters.get(parameter_name, default_value)

    @classmethod
    def _get_value_if_not_null(cls, event, parameter, default_value):
        return event[parameter] if event[parameter] else default_value


class JwtRequest(Request):
    """
    A HTTP request with a JWT token for authorization

    Does not verify token claims (API Gateway handles this).
    """

    def __init__(self, event, header='Authorization'):
        super().__init__(event)
        self._token = self._decode_token(header)

    @property
    def token(self):
        """The decoded JWT token"""
        return self._token

    def _decode_token(self, header):
        raw_token = self.headers.get(header)
        if not raw_token:
            raise MissingToken()

        try:
            return jwt.decode(
                raw_token, algorithms=['HS256', 'RS256'], options={"verify_signature": False}
            )
        except jwt.exceptions.DecodeError as error:
            raise InvalidToken from error
