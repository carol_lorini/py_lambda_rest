"""REST endpoint module"""
from http import HTTPMethod

import jsonschema

from .abstract_lambda_handler import AbstractLambdaHandler
from .exceptions import (ForbiddenRequestError, MissingToken,
                         ObjectNotFoundError)
from .helper.logger import LOGGER
from .request import Request
from .response_builder import (BadRequestBuilder, ForbiddenBuilder,
                               InternalServerErrorBuilder,
                               MethodNotAllowedBuilder, NotFoundBuilder,
                               ResponseBuilder, UnauthorizedBuilder)


class Action:
    """A REST action"""

    def __init__(self, action, body_schema=None):
        self._function = action
        self._body_schema = body_schema

    def execute(self):
        """Executes the request action"""
        return self.function()

    @property
    def function(self):
        """The function called when the action is executed"""
        return self._function

    @property
    def body_schema(self):
        """The schema used to validate the request body"""
        return self._body_schema


class AbstractEndpoint(AbstractLambdaHandler):
    """Handles REST requests"""

    def __init__(self, base_path='', resource='', request_class=Request):
        self._base_path = base_path
        self._resource = resource
        self._request_class = request_class
        self.request = None
        self.actions = {}
        self.register_action(HTTPMethod.GET, self._base_path, self.list)
        self.register_action(HTTPMethod.GET, self.build_url(), self.retrieve)
        self.register_action(HTTPMethod.POST, self._base_path, self.create)
        self.register_action(HTTPMethod.DELETE, self.build_url(), self.delete)
        self.register_action(HTTPMethod.PATCH, self.build_url(), self.partial_update)
        self.register_action(HTTPMethod.PUT, self.build_url(), self.update)

    def process_event(self, event, context):
        """Processes the event as a HTTP request"""
        try:
            LOGGER.debug('Request: %s', event)
            self.request = self._request_class(event)
            response = self.process_request().build()
            LOGGER.debug('Response: %s', response)
            return response
        except MissingToken:
            LOGGER.exception('Missing token: %s', event)
            return UnauthorizedBuilder('Missing authentication token').build()
        except Exception:  # pylint: disable=broad-except
            LOGGER.exception('AbstractEndpoint exception. Event: %s', event)
            return InternalServerErrorBuilder().build()

    def process_request(self) -> ResponseBuilder:
        """Processes a request by forwarding it to the appropriate handler"""
        try:
            action = self.actions.get((self.request.method, self.request.resource))
            if not action:
                return MethodNotAllowedBuilder(self.request.method, self.request.resource)
            self.validate_request(action.body_schema)
            return self.execute_action(action)
        except jsonschema.ValidationError as error:
            return BadRequestBuilder(str(error))
        except ForbiddenRequestError as error:
            LOGGER.info('Forbidden: %s %s', self.request.method, self.request.path)
            return ForbiddenBuilder(str(error))
        except ObjectNotFoundError as error:
            LOGGER.info('Object not found: %s %s', self.request.method, self.request.path)
            return NotFoundBuilder(str(error))

    def create(self):
        """Handles create requests"""
        return MethodNotAllowedBuilder('POST', '/')

    def delete(self):
        """Handles delete requests"""
        return MethodNotAllowedBuilder('DELETE', '/{id}')

    def list(self):
        """Handles list requests"""
        return MethodNotAllowedBuilder('GET', '/')

    def retrieve(self):
        """Handles retrieve requests"""
        return MethodNotAllowedBuilder('GET', '/{id}')

    def update(self):
        """Handles update requests"""
        return MethodNotAllowedBuilder('PUT', '/{id}')

    def partial_update(self):
        """Handles partial update requests"""
        return MethodNotAllowedBuilder('PATCH', '/{id}')

    def register_action(self, http_method, path, action, body_schema=None):
        """Registers an action to handle a HTTP request"""
        self.actions[(http_method, path)] = Action(action, body_schema)

    def build_url(self, path=''):
        """Builds the resource URL"""
        return self._base_path + '/{' + self._resource + '_id}' + path

    def validate_request(self, schema):
        """
        Checks if the request is valid before executing it. Implement body, permission, etc,
        checks here. Raise exception if request should not be executed.
        """
        self._validate_body(schema)

    def execute_action(self, action):
        """Executes the request."""
        return action.execute()

    def _validate_body(self, schema):
        if schema is None:
            return
        jsonschema.validate(
            self.request.body, schema, format_checker=jsonschema.FormatChecker()
        )
