"""Abstractions to use class as entrypoint to lambda function"""
import abc
import sys

_DISABLED = set()


def disable(func):
    """Disables decorator"""
    _DISABLED.add(func)


def enable(func):
    """Enables decorator"""
    _DISABLED.discard(func)


def lambda_handler(func):
    """Creates dynamic handler in caller's module called MyCallingClassHandler"""
    if lambda_handler in _DISABLED:
        return func

    module = func.__module__
    handler_name = f'{func.__name__}Handler'
    setattr(sys.modules[module], handler_name, func())
    return func


class AbstractLambdaHandler(abc.ABC):
    """Abstract lambda function handler"""

    def __call__(self, event, context):
        return self.process_event(event, context)

    @abc.abstractmethod
    def process_event(self, event, context):
        """Process event."""
