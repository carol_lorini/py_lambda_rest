"""HTTP response builders"""
from .exceptions import InvalidHttpStatusCode, MissingHttpStatusCode
from .helper.json_helper import JsonHelper


def enable_cors(allowed_origin='*'):
    """Enables CORS in all response builders. Deprecated method, use CORSConfigurator instead."""
    CORSConfigurator.enable_cors(allowed_origin)


def disable_cors():
    """Disables CORS in all response builders. Deprecated method, use CORSConfigurator instead."""
    CORSConfigurator.disable_cors()


class CORSConfigurator:
    """Configures CORS for all response builders in this module."""
    _CORS_ORIGIN = None

    @classmethod
    def enable_cors(cls, allowed_origin='*'):
        """Enables CORS in all response builders"""
        cls._CORS_ORIGIN = allowed_origin

    @classmethod
    def disable_cors(cls):
        """Disables CORS in all response builders"""
        cls._CORS_ORIGIN = None

    @classmethod
    def allowed_origins(cls):
        """Gets the allowed origins for cross-origin requests"""
        return cls._CORS_ORIGIN


class ResponseBuilder:
    """Builds a response to a HTTP request"""

    def __init__(self):
        self._is_base64_encoded = False
        self._status_code = None
        self._headers = {}
        self._multi_value_headers = {}
        self._body = None

        if CORSConfigurator.allowed_origins():
            self.header('Access-Control-Allow-Origin', CORSConfigurator.allowed_origins())

    def base64_encode(self, value):
        """Configures the response as base64 encoded. Default value is not encoded."""
        self._is_base64_encoded = value
        return self

    def body(self, value, json_encode=True):
        """Sets the response body. Also configures the content type header if the body is a json."""
        if json_encode:
            self._body = JsonHelper.dumps(value) if value is not None else None
            self.header("Content-Type", "application/json")
        else:
            self._body = value
        return self

    def header(self, name, value):
        """Adds a header to the response."""
        self._headers[name] = value
        return self

    def status_code(self, value):
        """Sets the response HTTP status code. Raises InvalidHttpStatusCode on error."""
        try:
            self._status_code = int(value)
        except ValueError as error:
            raise InvalidHttpStatusCode() from error
        return self

    def build(self):
        """Serializes the response to the expected format. Raises MissingHttpStatusCode."""
        if not self._status_code:
            raise MissingHttpStatusCode()

        response = {
            "isBase64Encoded": self._is_base64_encoded,
            "statusCode": self._status_code,
            "headers": self._headers,
            "multiValueHeaders": self._multi_value_headers,
            "body": self._body
        }
        return response


class OkBuilder(ResponseBuilder):
    """Builds an Ok HTTP response"""

    def __init__(self, body, json_encode=True):
        super().__init__()
        self.status_code(200)
        self.body(body, json_encode)


class CreatedBuilder(ResponseBuilder):
    """Builds a created HTTP response"""

    def __init__(self, body=None, json_encode=True):
        super().__init__()
        self.status_code(201)
        self.body(body, json_encode)


class NoContentBuilder(ResponseBuilder):
    """Builds a no content HTTP response"""

    def __init__(self):
        super().__init__()
        self.status_code(204)


class BadRequestBuilder(ResponseBuilder):
    """Builds a bad request HTTP response"""

    def __init__(self, error_message):
        super().__init__()
        self.status_code(400)
        self.body({"error": error_message})


class UnauthorizedBuilder(ResponseBuilder):
    """Builds an unauthorized HTTP response"""

    def __init__(self, error_message):
        super().__init__()
        self.status_code(401)
        self.body({"error": error_message})


class ForbiddenBuilder(ResponseBuilder):
    """Builds a forbidden HTTP response"""

    def __init__(self, error_message):
        super().__init__()
        self.status_code(403)
        self.body({"error": error_message})


class NotFoundBuilder(ResponseBuilder):
    """Builds a not found HTTP response"""

    def __init__(self, error_message):
        super().__init__()
        self.status_code(404)
        self.body({"error": error_message})


class MethodNotAllowedBuilder(ResponseBuilder):
    """Builds a method not allowed HTTP response"""

    def __init__(self, method, resource):
        super().__init__()
        self.status_code(405)
        message = f"Method {method} is not allowed on {resource}"
        self.body({"error": message})


class UnprocessableEntityBuilder(ResponseBuilder):
    """Builds an unprocessable entity HTTP response"""

    def __init__(self, error_message):
        super().__init__()
        self.status_code(422)
        self.body({"error": error_message})


class InternalServerErrorBuilder(ResponseBuilder):
    """Builds an internal server error HTTP response"""

    def __init__(self):
        super().__init__()
        self.status_code(500)
        self.body({"error": "Internal server error"})
