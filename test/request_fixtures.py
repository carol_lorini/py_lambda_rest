"""Common test fixtures module"""
from py_lambda_rest.helper.request_builder import JwtRequestBuilder, RequestBuilder

POST_REQUEST = RequestBuilder("/fake").post({"message": "this is a body"}) \
                                      .header("Content-Type", "application/json") \
                                      .header("Accepts", "gzip") \
                                      .query_param("name", "me") \
                                      .query_param("limit", "5") \
                                      .path_param("id", "10") \
                                      .build()

GET_REQUEST = RequestBuilder("/fake").query_param("name", "me").query_param("name", "you").build()

JWT_REQUEST = JwtRequestBuilder("/fake").token_param('user_id', '12345678').build()

INVALID_JWT_REQUEST = RequestBuilder("/fake").header('Authorization', 'abcd').build()

BINARY_POST_REQUEST = RequestBuilder("/fake").post_binary("aGVsbG8gd29ybGQ=").build()
