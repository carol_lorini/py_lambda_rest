"""Response builder testing module"""
import unittest

from py_lambda_rest import (BadRequestBuilder, CreatedBuilder,
                            ForbiddenBuilder, InternalServerErrorBuilder,
                            MethodNotAllowedBuilder, NoContentBuilder,
                            NotFoundBuilder, OkBuilder, ResponseBuilder,
                            UnprocessableEntityBuilder)
from py_lambda_rest.exceptions import (InvalidHttpStatusCode,
                                       MissingHttpStatusCode)
from py_lambda_rest.response_builder import disable_cors, enable_cors


class ResponseBuilderTest(unittest.TestCase):
    def setUp(self):
        self.builder = ResponseBuilder()

    def tearDown(self):
        disable_cors()

    def test_build_with_status_code_should_succeed(self):
        self.builder.status_code(200).build()

    def test_build_without_status_code_should_raise_exception(self):
        with self.assertRaises(MissingHttpStatusCode):
            self.builder.build()

    def test_status_code_with_string_should_raise_exception(self):
        with self.assertRaises(InvalidHttpStatusCode):
            self.builder.status_code("abcde")

    def test_build_with_headers_should_succeed(self):
        response = self.builder.header("Authorization", "token") \
                               .header("Content-Type", "application/json") \
                               .status_code(200) \
                               .base64_encode(False) \
                               .build()
        self.assertEqual("application/json", response['headers']['Content-Type'])
        self.assertEqual("token", response['headers']['Authorization'])
        self.assertEqual(200, response['statusCode'])

    def test_build_with_json_body_should_set_content_header(self):
        body = {'some_key': 'some_value'}
        response = self.builder.body(body).status_code(200).build()
        self.assertEqual("application/json", response['headers']['Content-Type'])

    def test_json_body_with_none_should_set_content_header(self):
        response = self.builder.body(None).status_code(200).build()
        self.assertEqual("application/json", response['headers']['Content-Type'])
        self.assertIsNone(response['body'])

    def test_build_with_non_json_body_should_succeed(self):
        body = 'a raw string body'
        response = self.builder.body(body, json_encode=False).status_code(200).build()
        self.assertEqual(body, response['body'])
        self.assertNotIn('Content-Type', response['headers'])

    def test_build_with_cors_enabled_should_add_cors_header(self):
        enable_cors('*.example.com')
        builder = ResponseBuilder()
        response = builder.status_code(200).build()
        self.assertEqual(response['headers']['Access-Control-Allow-Origin'], '*.example.com')

    def test_build_with_cors_disabled_should_not_add_cors_header(self):
        builder = ResponseBuilder()
        response = builder.status_code(200).build()
        self.assertNotIn('Access-Control-Allow-Origin', response['headers'])


class OkBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        body = {"abcd": "12345"}
        response = OkBuilder(body).build()
        self.assertEqual(200, response['statusCode'])
        self.assertIn("12345", response['body'])
        self.assertEqual("application/json", response['headers']['Content-Type'])


class CreatedBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        body = {"abcd": "12345"}
        response = CreatedBuilder(body).build()
        self.assertEqual(201, response['statusCode'])
        self.assertIn("12345", response['body'])
        self.assertEqual("application/json", response['headers']['Content-Type'])


class NoContentBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        response = NoContentBuilder().build()
        self.assertEqual(204, response['statusCode'])
        self.assertIsNone(response['body'])


class BadRequestBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        message = "There is a problem with the request"
        response = BadRequestBuilder(error_message=message).build()
        self.assertEqual(400, response['statusCode'])
        self.assertIn(message, response['body'])


class ForbiddenBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        message = "Forbidden request"
        response = ForbiddenBuilder(error_message=message).build()
        self.assertEqual(403, response['statusCode'])
        self.assertIn(message, response['body'])


class NotFoundBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        message = "Resouce not found"
        response = NotFoundBuilder(error_message=message).build()
        self.assertEqual(404, response['statusCode'])
        self.assertIn(message, response['body'])


class UnprocessableEntityBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        message = "There is a problem with the request"
        response = UnprocessableEntityBuilder(error_message=message).build()
        self.assertEqual(422, response['statusCode'])
        self.assertIn(message, response['body'])


class MethodNotAllowedBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        response = MethodNotAllowedBuilder(method='POST', resource='test').build()
        self.assertEqual(405, response['statusCode'])
        self.assertIn('Method POST is not allowed on test', response['body'])


class InternalServerErrorBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        response = InternalServerErrorBuilder().build()
        self.assertEqual(500, response['statusCode'])
        self.assertIn('Internal server error', response['body'])
