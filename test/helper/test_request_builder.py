"""Request builder testing module"""
import unittest

from py_lambda_rest.helper import JwtRequestBuilder, RequestBuilder


class ResquestBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        request = RequestBuilder().build()
        self.assertEqual(request['httpMethod'], 'GET')

    def test_build_with_post_should_succeed(self):
        request = RequestBuilder().post().build()
        self.assertEqual(request['httpMethod'], 'POST')

    def test_build_with_put_should_succeed(self):
        request = RequestBuilder().put().build()
        self.assertEqual(request['httpMethod'], 'PUT')

    def test_build_with_patch_should_succeed(self):
        request = RequestBuilder().patch().build()
        self.assertEqual(request['httpMethod'], 'PATCH')

    def test_build_with_delete_should_succeed(self):
        request = RequestBuilder().delete().build()
        self.assertEqual(request['httpMethod'], 'DELETE')


class JwtResquestBuilderTest(unittest.TestCase):
    def test_build_should_succeed(self):
        request = JwtRequestBuilder().build()
        self.assertEqual(request['httpMethod'], 'GET')
