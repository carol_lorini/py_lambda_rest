"""JsonHelper testing module"""
import datetime
import decimal
import unittest
from dataclasses import dataclass

import freezegun

from py_lambda_rest.helper.json_helper import JsonHelper


@dataclass
class FakeData:
    value: int = 0
    name: str = 'abcde'


class JsonHelperTest(unittest.TestCase):
    def test_dumps_with_date_object_should_succeed(self):
        date_object = datetime.date(1969, 7, 20)
        json = JsonHelper.dumps(date_object)
        self.assertEqual(json, '"1969-07-20"')

    def test_dumps_with_datetime_object_should_succeed(self):
        date_object = datetime.datetime(1969, 7, 20, 20, 17)
        json = JsonHelper.dumps(date_object)
        self.assertEqual(json, '"1969-07-20T20:17:00"')

    @freezegun.freeze_time("2019-01-01 03:42:34.001")
    def test_dumps_with_datetime_with_seconds_should_succeed(self):
        date_object = datetime.datetime.now()
        json = JsonHelper.dumps(date_object)
        self.assertEqual(json, '"2019-01-01T03:42:34"')

    def test_dumps_with_decimal_object_should_succeed(self):
        pie = decimal.Decimal(3.1427)
        json = JsonHelper.dumps(pie)
        self.assertEqual(json, '3.1427')

    def test_dumps_with_dictionary_should_succeed(self):
        test_dict = {
            'commander': 'Neil Armstrong',
            'pilot': 'Buzz Aldrin',
            'landing_time': datetime.datetime(1969, 7, 20, 20, 17),
        }
        json = JsonHelper.dumps(test_dict, sort_keys=True)
        expected = '{"commander": "Neil Armstrong", "landing_time": "1969-07-20T20:17:00", ' \
            '"pilot": "Buzz Aldrin"}'
        self.assertEqual(json, expected)

    def test_dumps_with_dataclass_should_succeed(self):
        data = FakeData()
        json = JsonHelper.dumps(data)
        expected = '{"value": 0, "name": "abcde"}'
        self.assertEqual(json, expected)
