"""Request testing module"""
import unittest
from http import HTTPMethod
from test.request_fixtures import (BINARY_POST_REQUEST, GET_REQUEST, INVALID_JWT_REQUEST,
                                   JWT_REQUEST, POST_REQUEST)

from py_lambda_rest.exceptions import InvalidToken, MissingToken
from py_lambda_rest.request import JwtRequest, Request


class RequestTest(unittest.TestCase):
    def test_constructor_with_valid_event_should_parse_request(self):
        request = Request(POST_REQUEST)
        self.assertEqual(request.method, HTTPMethod.POST)
        self.assertEqual(request.path, "/fake")
        self.assertEqual(request.resource, "/fake")
        self.assertEqual(len(request.headers), 2)
        self.assertEqual(len(request.query_parameters), 2)
        self.assertEqual(len(request.path_paramters), 1)
        self.assertEqual(request.body['message'], "this is a body")

    def test_get_header_with_existing_header_should_return_header_value(self):
        request = Request(POST_REQUEST)
        self.assertEqual(request.get_header('Content-Type'), "application/json")

    def test_get_header_with_non_existing_header_should_return_none(self):
        request = Request(POST_REQUEST)
        self.assertIsNone(request.get_header('Made-up-header'))

    def test_get_header_with_non_existing_header_should_return_default_value(self):
        request = Request(POST_REQUEST)
        self.assertEqual('some_value', request.get_header('Made-up-header', 'some_value'))

    def test_get_query_param_with_existing_param_should_return_value(self):
        request = Request(POST_REQUEST)
        self.assertEqual(request.get_query_parameter('name'), "me")

    def test_get_query_param_with_non_existing_param_should_return_none(self):
        request = Request(POST_REQUEST)
        self.assertIsNone(request.get_query_parameter('Made-up-param'))

    def test_get_query_param_with_non_existing_param_should_return_default_value(self):
        request = Request(POST_REQUEST)
        self.assertEqual('default', request.get_query_parameter('Made-up-param', 'default'))

    def test_get_path_param_with_existing_param_should_return_value(self):
        request = Request(POST_REQUEST)
        self.assertEqual(request.get_path_parameter('id'), "10")

    def test_get_path_param_with_non_existing_param_should_return_none(self):
        request = Request(POST_REQUEST)
        self.assertIsNone(request.get_path_parameter('Made-up-param'))

    def test_get_path_param_with_non_existing_param_should_return_default_value(self):
        request = Request(POST_REQUEST)
        self.assertEqual('default', request.get_path_parameter('Made-up-param', 'default'))

    def test_constructor_with_null_body_should_succeed(self):
        request = Request(GET_REQUEST)
        self.assertIsNone(request.body)

    def test_constructor_with_base64_body_should_not_decode_body(self):
        request = Request(BINARY_POST_REQUEST)
        self.assertTrue(request.is_base64)
        self.assertIsNotNone(request.body)

    def test_get_query_param_with_no_query_params_should_return_none(self):
        request = Request(GET_REQUEST)
        self.assertIsNone(request.get_query_parameter('some_parameter'))

    def test_get_multi_value_query_param_should_succeed(self):
        request = Request(GET_REQUEST)
        values = request.get_multi_value_query_parameter('name')
        self.assertEqual(len(values), 2)
        self.assertEqual(values[0], 'me')
        self.assertEqual(values[1], 'you')

    def test_get_multi_value_query_param_with_single_value_should_succeed(self):
        request = Request(POST_REQUEST)
        self.assertIn('limit', request.multi_value_query_parameters)
        self.assertEqual(['5'], request.get_multi_value_query_parameter('limit'))


class JwtRequestTest(unittest.TestCase):
    def test_constructor_with_valid_token_should_decode_token(self):
        request = JwtRequest(JWT_REQUEST)
        self.assertIsNotNone(request)
        self.assertEqual(request.token.get('user_id'), '12345678')

    def test_with_missing_token_should_raise_exception(self):
        with self.assertRaises(MissingToken):
            JwtRequest(GET_REQUEST)

    def test_with_invalid_token_should_raise_exception(self):
        with self.assertRaises(InvalidToken):
            JwtRequest(INVALID_JWT_REQUEST)
