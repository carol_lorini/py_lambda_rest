"""Abstract endpoint testing module"""
import unittest
from http import HTTPMethod
from test.request_fixtures import GET_REQUEST, JWT_REQUEST, POST_REQUEST

from py_lambda_rest import AbstractEndpoint, JwtRequest, NoContentBuilder
from py_lambda_rest.exceptions import ForbiddenRequestError, ObjectNotFoundError
from py_lambda_rest.helper.logger import LOGGER
from py_lambda_rest.helper.request_builder import RequestBuilder

TEST_SCHEMA = {
    "$schema": "http://json-schema.org/draft-06/schema#",
    "type": "object",
    "required": ["name", "birthday"],
    "properties": {
        "name": {"type": "string"},
        "codename": {"type": "string"},
        "birthday": {"type": "string", "format": "date"}
    }
}


class FakeEndpoint(AbstractEndpoint):
    def __init__(self):
        super().__init__(base_path='/fake', resource='fake')
        self.register_action(HTTPMethod.POST, '/fake/schema_test', self.schema_test, TEST_SCHEMA)
        self.register_action(HTTPMethod.GET, '/fake/forbidden', self.forbidden)

    def create(self):
        raise Exception()  # pylint: disable=broad-exception-raised

    def list(self):
        raise ObjectNotFoundError('fake_resource', 10)

    def forbidden(self):
        raise ForbiddenRequestError('Forbidden request')

    def schema_test(self):
        return NoContentBuilder()


class FakeJWTEndpoint(AbstractEndpoint):
    def __init__(self):
        super().__init__(base_path='/fake', resource='fake', request_class=JwtRequest)

    def list(self):
        return NoContentBuilder()


class AbstractEndpointTest(unittest.TestCase):
    def setUp(self):
        LOGGER.setLevel('CRITICAL')

    def test_process_event_with_generic_exception_should_return_500(self):
        response = FakeEndpoint().process_event(POST_REQUEST, None)
        self.assertEqual(response['statusCode'], 500)

    def test_process_event_with_object_not_found_error_should_return_404(self):
        response = FakeEndpoint().process_event(GET_REQUEST, None)
        self.assertEqual(response['statusCode'], 404)
        self.assertIn('fake_resource 10 not found', response['body'])

    def test_process_event_with_forbidden_error_should_return_403(self):
        request = RequestBuilder('/fake/forbidden').build()
        response = FakeEndpoint().process_event(request, None)
        self.assertEqual(response['statusCode'], 403)

    def test_process_event_with_body_schema_and_valid_body_should_succeed(self):
        body = {
            "name": "Steve Rogers",
            "birthday": "1918-07-04",
            "codename": "Captain America"
        }
        request = RequestBuilder('/fake/schema_test').post(body).build()
        response = FakeEndpoint().process_event(request, None)
        self.assertEqual(response['statusCode'], 204)

    def test_process_event_with_body_schema_and_invalid_body_should_return_400(self):
        body = {
            "name": "Steve Rogers",
        }
        request = RequestBuilder('/fake/schema_test').post(body).build()
        response = FakeEndpoint().process_event(request, None)
        self.assertEqual(response['statusCode'], 400)

    def test_process_event_with_unregistered_path_should_return_405(self):
        request = RequestBuilder('/fake/wrong_path').post().build()
        response = FakeEndpoint().process_event(request, None)
        self.assertEqual(response['statusCode'], 405)

    def test_process_event_with_unregistered_method_should_return_405(self):
        request = RequestBuilder('/fake/{fake_id}').put().build()
        response = FakeEndpoint().process_event(request, None)
        self.assertEqual(response['statusCode'], 405)

    def test_process_event_with_token_should_return_204(self):
        response = FakeJWTEndpoint().process_event(JWT_REQUEST, None)
        self.assertEqual(response['statusCode'], 204)

    def test_process_event_with_missing_token_should_return_401(self):
        request = RequestBuilder('/fake').build()
        response = FakeJWTEndpoint().process_event(request, None)
        self.assertEqual(response['statusCode'], 401)
        self.assertIn('Missing authentication token', response['body'])
